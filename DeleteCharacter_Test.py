import allure
import SendRequest


class TestDeleteCharacter():

    @allure.feature('Удалить персонажа по имени')
    def test_deleteCharacterByName(self, character):
        with allure.step('Send Delete Request'):
            r = SendRequest.deleteCharacterByName(character['name'])
            assert r.status_code == 200
            # expected result : {"result":["5d7a9a75b072d010b9f386b8 is deleted"]}
            assert ('is deleted' in r.text)
            allure.attach(r.content.decode(), character['name'])

        with allure.step('Trying to get deleted item'):
            r = SendRequest.getCharacterByName(character['name'])
            assert r.status_code == 200
            assert r.content.decode().strip() == '{"result":"No such name"}'
            allure.attach(r.content.decode(), character['name'])

    @allure.feature('Попытка удалить несуществующего персонажа')
    def test_deleteNonExistingCharacter(self):
        name = 'NotExisting'
        with allure.step('Send Delete Request'):
            r = SendRequest.deleteCharacterByName(name)
            assert r.status_code == 200
        allure.attach(r.content.decode(), name)
        with allure.step('Validate Response'):
            # expected result is {"result": ["No such name"]}.
            # this is the mystery why is there the array
            assert r.content.decode().strip() == '{"result":["No such name"]}'
