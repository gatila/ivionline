import pytest
import allure
import json
import csv


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    if rep.when == "call" and rep.failed:
        allure.attach(rep.nodeid, "FAIL")


@pytest.fixture()
def get_character_data():
    with open('data/characters.csv', newline='', encoding='utf-8') as csvfile:
        return list(csv.DictReader(csvfile))


@pytest.fixture()
def character():
    with open('data/character.json', 'r') as f:
        return json.loads(f.read())
