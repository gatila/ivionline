import allure
import SendRequest
import json
from random import randrange
from BaseTest import BaseTest
import Utils


class TestGetCharacter(BaseTest):

    @allure.feature('Получить список персонажей')
    def test_getCharacterList(self):
        with allure.step('Send Get Request'):
            r = SendRequest.getCharacters()
            assert r.status_code == 200
            data = json.loads(r.text)
            allure.attach(r.content.decode(), 'RESPONSE CONTENT')

        with allure.step('Validate Response'):
            assert Utils.assert_valid_list_of_character_schema(data)

        l = len(data['result'])
        with open('data/character.json', 'w') as f:
            f.write(json.dumps(data['result'][randrange(0, l)]))

    @allure.feature('Получить персонажа по имени')
    def test_getCharacterByName(self):
        with open('data/character.json', 'r') as f:
            character = json.loads(f.read())

        with allure.step('Send Get Request'):
            r = SendRequest.getCharacterByName(character['name'])
            assert r.status_code == 200

        with allure.step('Validate Response'):
            allure.attach(r.content.decode(), character['name'])
            response_content = json.loads(r.content)
            assert len(response_content['result']) == 1
            assert Utils.assert_valid_character_schema(response_content['result'][0])
