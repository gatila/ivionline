import requests
import data.account as account
import json

host = 'rest.test.ivi.ru'
httpBaseUrl = 'http://' + host + '/character'
httpsBaseUrl = 'https://' + host + '/character'
getCharactersUrl = 'http://' + host + '/characters'
getResetUrl = 'http://' + host + '/reset'


def getCharacters():
    response = requests.get(getCharactersUrl, auth=(account.login, account.password))
    return response


def getCharacterByName(name):
    response = requests.get(httpBaseUrl + '/' + name, auth=(account.login, account.password))
    return response


def deleteCharacterByName(name):
    response = requests.delete(httpBaseUrl + '/' + name, auth=(account.login, account.password))
    assert response.status_code == 200
    return response


def postCharacter(data):
    headers = {'Content-type': 'application/json'}
    response = requests.post(httpBaseUrl, data=data, headers=headers, auth=(account.login, account.password), timeout = 10)
    return response


def putCharacter(data, name=''):
    if name == '':
        json_data = json.loads(data)
        name = json_data['name']
    headers = {'Content-type': 'application/json'}
    response = requests.put(httpBaseUrl + '/' + name, data=data, headers=headers,
                            auth=(account.login, account.password))
    return response


def reset():
    return requests.post(getResetUrl, auth=(account.login, account.password))
