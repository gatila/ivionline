import csv
import random

universe = ['Warcraft', 'Dungeons and Dragons', 'Star Wars', 'Star Trek', 'Dune', 'The Wizard of Oz', 'Narnia',
            'Westeros', 'Middle-Earth', 'Earthsea', 'Foundation']
identity = 'Secret'
education = "We don't need no education"

with open('data/charactersList.csv', 'w', newline='', encoding='utf-8') as csvfile:
    fieldnames = ["education", "height", "identity", "name", "other_aliases", "universe", "weight"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    with open('data/names.csv', 'r', newline='') as nf:
        reader = csv.reader(nf)
        for row in reader:
            name = " ".join(row)
            # name,education,other_aliases,universe,height,weight,identity
            writer.writerow(
                {'name': name, 'education': education, 'other_aliases': '',
                 'universe': random.choice(universe), 'height': random.randint(140, 210),
                 'weight': random.randint(40, 180)})
