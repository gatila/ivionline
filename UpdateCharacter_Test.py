import pytest
import csv
import allure
import SendRequest
import json
import Utils


def get_characterPUT_data():
    with open('data/charactersPUT.csv', newline='', encoding='utf-8') as csvfile:
        return list(csv.DictReader(csvfile))


class TestUpdateCharacter():

    @allure.feature('Апдейт персонажа метод PUT')
    @pytest.mark.parametrize('data', get_characterPUT_data())
    def test_update_character(self, data):
        json_data = json.dumps(data)
        # allure.attach(data ,data['name'], allure.attach('TEXT'))
        with allure.step('Send Put Request'):
            resp = SendRequest.putCharacter(json_data)
            allure.attach(resp.content, data['name'])
        with allure.step('Check Response'):
            assert resp.status_code == 200
            response_content = json.loads(resp.text)
            assert type(response_content['result'][0]) == dict
            # assert Utils.assert_valid_character_schema(response_content['result'])

        with allure.step('Get Created Character'):
            r = SendRequest.getCharacterByName(data['name'])
            allure.attach(r.content, data['name'])
            assert r.status_code == 200
            response_content = json.loads(r.content)
            assert len(response_content['result']) == 1
            assert type(response_content['result'][0]) == dict
            # assert Utils.assert_valid_character_schema(response_content['result'][0])
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json.loads(json_data), response_content['result'][0]), '%s is not equals %s' % (
                json_data, response_content['result'][0])

    @allure.feature('Апдейт имени персонажа')
    def test_update_character_name(self):
        oldName = '3-D Man'
        newName = '3-D Man UPDATED'
        data = {"education": "", "height": "", "identity": "", "name": newName, "other_aliases": "", "universe": "",
                "weight": ""}
        request_data = json.dumps(data)
        with allure.step('Send Put Request'):
            resp = SendRequest.putCharacter(request_data, name=oldName)
            allure.attach(resp.content, data['name'])
        with allure.step('Check Response'):
            assert resp.status_code == 200
            response_content = json.loads(resp.text)
            assert len(response_content['result']) == 1
            assert type(response_content['result'][0]) == dict
            assert (response_content['result'][0]['name'] == newName), "Name was not updated"

    # this test fails , server returns 500 http code and no error description
    @allure.feature('Не полный набор параметров')
    @pytest.mark.xfail()
    def test_create_without_identity_param(self):
        with open('data/character.json', 'r') as f:
            character = json.loads(f.read())
        del character['identity']
        request_data = json.dumps(character)
        with allure.step('Send Put Request'):
            resp = SendRequest.putCharacter(request_data)
            allure.attach(resp.text, character['name'])
            assert resp.status_code == 200, 'Status code is %s' % resp.status_code
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json.loads(request_data),
                                      resp['result']), '%s is not equals %s' % (
                request_data, resp['result'][0])

    #this test fails , server returns 500 http code and no error description
    @allure.feature('Не полный набор параметров')
    @pytest.mark.xfail()
    def test_create_without_weight(self):
        with open('data/character.json', 'r') as f:
            character = json.loads(f.read())
        del character['weight']
        request_data = json.dumps(character)
        with allure.step('Send Put Request'):
            resp = SendRequest.putCharacter(request_data)
            assert resp.status_code == 200, 'Status code is %s' % resp.status_code
            allure.attach(resp.text, character['name'])
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json.loads(request_data),
                                      resp['result']), '%s is not equals %s' % (
                request_data, resp['result'][0])
