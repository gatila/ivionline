import jsonschema
import json
import logging
import allure


def assert_valid_character_schema(data):
    schema = _load_json_schema('data/characterSchema.json')
    try:
        jsonschema.validate(data, schema)
        return True
    except jsonschema.ValidationError as e:
        logging.error(e.message)
        allure.attach(e.message, e.context, "VALIDATE ERROR")
    return False


def assert_valid_list_of_character_schema(data):
    schema = _load_json_schema('data/characterListSchema.json')
    try:
        jsonschema.validate(data, schema)
        return True
    except jsonschema.ValidationError as e:
        logging.error(e.message)
        allure.attach(e.message, e.context, "VALIDATE ERROR")
    return False


def _load_json_schema(file):
    with open(file) as schema_file:
        return json.loads(schema_file.read())


def compare_json(json1, json2):
    return sorted(json1.items()) == sorted(json2.items())
