import pytest
import csv
import allure
import SendRequest
import json
from BaseTest import BaseTest
import Utils


def get_character_data():
    with open('data/charactersJson.txt', newline='', encoding='utf-8') as file:
        array_data = []
        for row in file:
            asd = row.encode('utf-8').decode('utf8').strip()
            array_data.append(asd)
        return array_data


class TestCreateCharacters(BaseTest):

    @allure.feature('Создание персонажа метод POST')
    @pytest.mark.parametrize('data', get_character_data())
    def test_create_character(self, data):
        json_data = json.loads(data)
        with allure.step('Send Post Request'):
            resp = SendRequest.postCharacter(data.encode('utf-8'))
            allure.attach(resp.text, json_data['name'])
        with allure.step('Check Response Code'):
            assert resp.status_code == 200
            response_content = json.loads(resp.content.decode())
            assert type(response_content['result']) == dict
        with allure.step('Validate Response'):
            assert Utils.assert_valid_character_schema(response_content['result'])
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json_data, response_content['result']), '%s is not equals %s' % (
                json_data, response_content['result'][0])

        with allure.step('Get Created Character'):
            r = SendRequest.getCharacterByName(json_data['name'])
            allure.attach(r.content, json_data['name'])
            assert r.status_code == 200
            response_content = json.loads(r.content)

            assert len(response_content['result']) == 1
            assert type(response_content['result'][0]) == dict
            assert Utils.assert_valid_character_schema(response_content['result'][0])
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json_data,
                                      response_content['result'][0]), '%s is not equals %s' % (
                json_data, response_content['result'][0])

    @allure.feature('Создание уже существующего персонажа метод POST')
    def test_create_double_character(self, character):
        request_data = json.dumps(character)
        with allure.step('Send Post Request'):
            resp = SendRequest.postCharacter(request_data)
            allure.attach(resp.text, character['name'])
            # expected result : {"%Name% already exists"]}
            response_data = json.loads(resp.text)
            expected_string = '%s is already exists' % character['name']
            assert response_data['result'] == expected_string

    @allure.feature('Не полный набор параметров')
    @pytest.mark.xfail()
    def test_create_without_identity_param(self):
        with open('data/character.json', 'r') as f:
            character = json.loads(f.read())
        character['name'] = 'New Name'
        del character['identity']
        request_data = json.dumps(character)
        with allure.step('Send Post Request'):
            resp = SendRequest.postCharacter(request_data)
            allure.attach(resp.text, character['name'])
            assert resp.status_code == 200, 'Status code is %s' % resp.status_code
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json.loads(request_data),
                                      resp['result']), '%s is not equals %s' % (
                request_data, resp['result'][0])

    @allure.feature('Не полный набор параметров')
    @pytest.mark.xfail()
    def test_create_without_weight(self):
        with open('data/character.json', 'r') as f:
            character = json.loads(f.read())
        character['name'] = 'New Name 2'
        del character['weight']
        request_data = json.dumps(character)
        with allure.step('Send Post Request'):
            resp = SendRequest.postCharacter(request_data)
            assert resp.status_code == 200, 'Status code is %s' % resp.status_code
            allure.attach(resp.text, character['name'])
        with allure.step('Check All Fields'):
            assert Utils.compare_json(json.loads(request_data),
                                      resp['result']), '%s is not equals %s' % (
                request_data, resp['result'][0])
