# Test For Ivi

Thank to my cat, he helped me very much

## Prepare

Install pytest, allure, jsonschema, requests

## Run

### Run All Test By Command
```
pytest -v --alluredir=allure-results
```

### Run One Test By Command
```
pytest -v [FileName.py] --alluredir=allure-results
```