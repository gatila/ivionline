import SendRequest
import csv
import pytest


class BaseTest():
    def setup_class(self):
        SendRequest.reset()

    def teardown_class(self):
        SendRequest.reset()
