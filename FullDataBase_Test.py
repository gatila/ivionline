import csv
import allure
import SendRequest
import json
import time
from BaseTest import BaseTest


class TestCreateCharacters(BaseTest):

    @allure.feature('Заполняем БД')
    def test_full_database(self):
        with allure.step('Count start items'):
            r = SendRequest.getCharacters()
            assert r.status_code == 200
            json_data = json.loads(r.text)
            current_count = len(json_data['result'])
            allure.attach(str(current_count), 'COUNT')
        with allure.step('Fill in DataBase'):
            with open('data/charactersList.csv', newline='') as f:
                reader = csv.DictReader(f)
                while current_count < 500:
                    # Sometimes I got Connection Error, so sleep  (
                    time.sleep(0.1)
                    row = next(reader)
                    request_data = json.dumps(row)
                    resp = SendRequest.postCharacter(request_data)
                    assert resp.status_code == 200
                    current_count += 1
                request_data = json.dumps(row)
                resp = SendRequest.postCharacter(request_data)
                assert resp.status_code == 400
                expected_result = '{"error":"Collection can\'t contain more than 500 items"}'
                assert resp.text.strip() == expected_result.strip()
